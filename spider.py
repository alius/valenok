#!/usr/bin/python
from lib.core.engine import prepare, start, stop

def main():
    prepare()
    start()
    stop()

if __name__ == "__main__":
    main()
