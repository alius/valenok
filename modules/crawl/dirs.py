import requests, os, threading

from lib.core.data import urlQueue

class Dirs():

    def __init__(self, c):
        self.config = c
        self.L = list()

    def printInfo(self):
        print 'author: Artjom Vassiljev'
        print 'description: Brute-forces directories on the server'

    @staticmethod
    def bruteforce(num, links, container, is_debug):
        for link in links:
            link = link.strip("\n ")
            urlQueue.put_nowait(link) # show the crawled url in UI
            if is_debug: print 'Bruteforcing: %s' % link
            try:
                r = requests.head(link)
                if (r.status_code != 404): container.append(link)
            except Exception, e:
                if is_debug: print 'Exception in dirs module: %s' % e
                pass


    def run(self):
        url = self.config.url
        if not url.endswith('/'): url += '/'
        if self.config.dirs_wordlist:
            i = 0
            links = dict()
            for line in open(self.config.dirs_wordlist, 'r').readlines():
                if line.startswith('#'): continue
                if i % self.config.threads not in  links: links[i % self.config.threads] = list()
                links[i % self.config.threads].append(url + line)
                i += 1

            tlist = list()
            for i in range(self.config.threads):
                t = threading.Thread(target=Dirs.bruteforce, args=(i, links[i], self.L, self.config.debug))
                t.start()
                tlist.append(t)

            for i in range(self.config.threads):
                tlist[i].join()
                if self.config.debug: print 'Finished bruteforcing. Waiting to join threads'

        print 'Dirs module has finished'
        return self.L
