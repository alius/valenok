import requests

class Robots():

    def __init__(self, c):
        self.config = c

    def printInfo(self):
        print 'author: Artjom Vassiljev'
        print 'description: Parses robots.txt file'

    def run(self):
        L = list()
        url = self.config.url + 'robots.txt' if self.config.url.endswith('/') else self.config.url + '/robots.txt'
        r = requests.head(url)
        if (r.status_code != 404):
            r = requests.get(url)
            a = r.text.lower().split('\n')
            for line in a:
                if line.startswith('allow') or line.startswith('disallow'):
                    L.append(line.split(':')[1].split('#')[0].replace(' ', '')[1:])
        return L
