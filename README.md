Valenok
=======

Website crawler and spider. Yet another one.

Installation
-----
The only currently supported backend is Redis. You would need to install Redis-py which can be downloaded here: https://github.com/andymccurdy/redis-py. All network requests are using Requests library: http://www.python-requests.org. Other than that you should be ready to go.

Config file options
-----

### Domain
* url -- URL to start the crawling process. There is a bug which prevents from specifying the URL with leading http:// and trailing /. So write in the form of www.domain.com

### Database
* driver -- Database driver to use. Only Redis is currently supported
* host -- Host where the database is installed
* port -- Port to use
* db -- Database name

### Threading
* count -- Number of threads to use. Minimum is 1

### Socket
* timeout -- HTTP request timeout in seconds

### Crawler
* single_domain -- Whether to crawl only single domain or fetch links from the database when the current domain is finished.

### Reporting
* output -- True or false whether to print the report with crawled links
* driver -- Which format to use for report. Currently only txt is supported.
* path -- Path where to save the report

### Misc
* gui -- Use ncurses gui.
* resume -- if the scan was interrupted setting this to true will resume it.
* debug -- when set to true prints some debug information to stdout.
* modules -- comma-separated list of modules. Two modules are currently available: crawl.robots and crawl.dirs. First one parses robots.txt file if it exists, and dirs module bruteforces directories on the server.
* dirs_wordlist -- path to the dictionary used to brute-force directories if crawl.dirs module is enabled.
