import redis, json
from driver import Driver

class Redis_Driver(Driver):

    def __init__(self, params):
        try:
            self.r = redis.StrictRedis(host = params['host'], port = int(params['port']), db = int(params['db']))
        except KeyError:
            print 'Redis exception'

    def rename(self, key, new_key):
        self.r.rename(key, new_key)

    def queue_size(self):
        return self.r.scard('queue')

    def stats(self):
        return self.r.scard('visited')

    def queue(self, value):
        ''' dict is passed '''
        if value and not self.r.sismember('queue', value['uri']):
            # add url to the queue
            self.r.sadd('queue', value['uri'])
            # add remaining data to hash
            self.r.hmset(value['uri'], {'code': value['code'], 'uri': value['uri'], 'parent': value['parent']})

    def in_queue(self, value):
        return self.r.sismember('queue', value)

    def pop_queue(self):
        ''' dict is returned '''
        url = self.r.spop('queue')
        if url:
            try:
                o = self.r.hgetall(url)
                self.r.delete(url)
                return o
            except Exception:
                print 'EXCEPTION!!!'
                print url
                print 'EXCEPTION!!!'
        return None

    def clear_queue(self):
        m = self.r.smembers('queue')
        self.r.delete('queue')
        for member in m:
            try:
                print 'Deleting from db: ', member
                if member and self.r.hexists(member, 'uri'):
                    self.r.hdel(member, 'uri', 'code', 'parent')
            except Exception, e:
                print 'Exception: %s', e

    def visited(self, value):
        ''' string is passed '''
        if value: self.r.sadd('visited', value)

    def in_visited(self, value):
        ''' string is passed '''
        return self.r.sismember('visited', value)

    def get_visited(self):
        return self.r.smembers('visited')

    def get_report(self, key):
        s = self.r.smembers(key)
        l = list()
        for i in s: l.append(json.loads(i))
        return l

    def clear_visited(self):
        self.r.delete('visited')

    def save(self, key, value = None):
        ''' dict is passed '''
        if value:
            try:
                self.r.sadd(key, json.dumps(value, encoding="utf-8"))
            except UnicodeDecodeError, e:
                print 'Exception! json.dumps: ', value
 
    def next(self):
        keys = self.r.keys('*').split(' ')
        for key in keys:
            if self.r.scard(key) == 1 and key.endswith('.com'): return key
            else: continue
