class Driver(object):

    def rename(self, key, new_key):
        ''' Renames the key to another name
            Args:
                key: name of the key
                new_key: new name
            Returns:
                void
        '''
        raise NotImplementedError

    def queue_size(self):
        ''' Returns number of links in the queue '''
        raise NotImplementedError

    def stats(self):
        ''' Returns number of links in the visited queue
            Args:
                void
            Returns
                integer: number of links visited
        '''
        raise NotImplementedError

    def queue(self, value):
        ''' Saves the link to the queue
            Args:
                value: link to save to the queue
            Returns
                void
        '''
        raise NotImplementedError

    def in_queue(self, value):
        ''' Checks whether the link is in the queue
            Args:
                value: link to check
            Returns
                boolean: result
        '''
        raise NotImplementedError

    def pop_queue(self):
        ''' Returns a link from the queue
            Args:
                void
            Returns:
                string: url or None if the queue is empty
        '''
        raise NotImplementedError

    def clear_queue(self):
        ''' Clears the queue
            Args:
                void
            Returns:
                void
        '''
        raise NotImplementedError

    def visited(self, value):
        ''' Puts the link to the visited queue
            Args:
                value: visited link
            Returns:
                void
        '''
        raise NotImplementedError

    def in_visited(self, value):
        ''' Checks whether the provided url was already visited
            Args:
                value: visited link
            Returns:
                boolean: status
        '''
        raise NotImplementedError

    def clear_visited(self):
        ''' Clears the visited queue
            Args:
                void
            Returns:
                void
        '''
        raise NotImplementedError

    def get_visited(self):
        ''' Returns all visited links
            Args:
                void
            Returns:
                list
        '''
        raise NotImplementedError

    def get_report(self, key):
        ''' Returns list of dicts for the specific site
            Args:
                key: site url
            Returns:
                list of dicts
        '''

    def save(self, key, value = None):
        ''' Saves the link to the database. If the key does not
            exist it will be created
            Args:
                key: base website address used as a key
                value: relative url
            Returns:
                boolean depending on the success of the operation
        '''
        raise NotImplementedError

    def next(self, key = None):
        ''' Returns next link in the queue
            Args:
                key: base website address used as a key or None
            Returns:
                string with full url or null if the queue is empty
                in case the key is not none, otherwise returns random
                domain
        '''
        raise NotImplementedError
