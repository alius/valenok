import threading, curses, Queue

class UIthread(threading.Thread):

    def __init__(self, queue, timer):
        self.draw = True
        self.last_url = ""
        self.last_time = 0
        self.queue = queue
        self.timer = timer
        self.screen = curses.initscr()
        threading.Thread.__init__(self)

    def stop(self):
        curses.endwin()

    def run(self):
        while not curses.isendwin():
            updated = False
            try:
                url = self.queue.get_nowait()
                if url: self.last_url = url
                updated = True
            except Exception:
                pass

            try:
                elapsed = self.timer.get_nowait()
                if elapsed: self.last_time = elapsed
                updated = True
            except Exception:
                pass

            if (updated):
                self.screen.clear()
                self.screen.border(0)
                self.screen.addstr(4, 3, "Current URL: " + str(self.last_url))
                self.screen.addstr(5, 3, "Elapsed time: " + str(self.last_time))
                self.screen.refresh()
