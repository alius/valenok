from Driver import Driver

import os

class Txt(Driver):

    def __init__(self, params):
        try:
            if os.path.isdir(params['path']):
                if not params['path'].endswith('/'): params['path'] += '/'
                params['path'] += 'report.txt'
            self.f = open(params['path'], 'w')
        except IOError:
            print 'File exception'

    def create(self, data):
        for line in data:
            self.f.write('%s\n' % line.encode('UTF-8'))
        self.f.close()
