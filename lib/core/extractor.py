import re
import time
import sys

class Extractor():

    @staticmethod
    def extract_links(source, fast = False):
        L = list()
        E = ['https://ssl', 'javascript:', 'http://www', 'www', '://', '#', '/', 'https://ssl.', 'http://www.', 'www.', 'mailto:']
        STARTS = ['mailto', 'javascript', '#', '&quot;']
        ENDS = ['.css', '.js', '.jpg', '.gif', '.png', '.flv', '.mpeg', '.mp4', '.ico', '.dtd', '.jpeg', '.zip', '.rar', '.gz', '.gzip', '.bzip2', '.bzip', '.pdf', '.wmv', '.JPG', '.xls', '.doc', '.ppt']

        if fast:
            while source:
                try:
                    start = source.index('href') + 6
                    stop = start + source[start:].index('"')
                    # double check in case the string was terminated with single quote
                    if stop < start + source[start:].index('</'):
                        L.append(source[start:stop])
                    else:
                        # single quote
                        pass
                    source = source[stop:]
                except ValueError:
                    break

        else:
            # tries to find links within HREF tag
            regex = 'href\s?=\s?[\'"]?([^\'" >]+)'
            L = re.findall(regex, source, re.M | re.I)

            # during the first run tries to find any link using general pattern
            regex = 'http[s]?://[/a-zA-Z0-9\.\-_\?=;,%]+'
            L.extend(re.findall(regex, source, re.M | re.I))

            # tries to find www
            regex = 'www\.[a-zA-Z0-9\.\-\?=;,-]*'
            L.extend(re.findall(regex, source, re.M | re.I))

        # remove duplicates
        L = sorted(set(L))
        L = list(set(L) - set(E))

        # exclude links that start or end with registered words
        L = [link for link in L if not any(map(link.startswith,STARTS))]

        return [link for link in L if not any(map(link.endswith,ENDS))]
