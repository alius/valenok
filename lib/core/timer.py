import time, threading

class Timer(threading.Thread):

    def __init__(self, queue):
        self.queue = queue
        self.running = True
        self.elapsed = 0
        self.seconds = 0
        self.minutes = 0
        self.hours = 0
        self.start_time = time.time()
        threading.Thread.__init__(self)

    def stop(self):
        self.running = False

    def run(self):
        while self.running:
            self.elapsed = int(time.time() - self.start_time)
            self.hours = self.elapsed / 3600
            self.minutes = (self.elapsed / 60) % 60
            self.seconds = self.elapsed - self.minutes * 60
            self.queue.put_nowait(str(self.hours) + ":" + str(self.minutes) + ":" + str(self.seconds))
            time.sleep(1)

    def sec(self):
        return self.seconds

    def mins(self):
        return self.minutes

    def hrs(self):
        return self.hours
