import re, time, sys
from urlparse import urlparse
from threading import Lock

from lib.decorators import synchronized

lock = Lock()

class Manager():

    fullDomain = None
    singleDomain = True
    visitedLinks = set()

    def setup(self, db, config):
        self.db = db
        self.config = config

    ''' Sets the current crawlable domain '''
    def set_domain(self, domain):
        self.currentDomain = domain
        if domain.startswith('http://'): self.shortDomain = domain[7:]
        if self.shortDomain.startswith('www.'): self.shortDomain = self.shortDomain[4:]

    ''' Specifies whether we should crawl a single domain or not '''
    def crawl_single(self, single = True):
        self.singleDomain = single

    ''' Adds single url to the list '''
    def add(self, url):
        url['uri'] = url['uri'].replace('%3A', ':').replace('%2F', '/').replace('%3F', '?').replace('%3D', '=')

        uri = url['uri']
        if uri.startswith('http://'): uri = uri[7:]
        if uri.startswith('www.'): uri = uri[4:]

        if not url['uri'].startswith("www") and not url['uri'].startswith("http"):
            if re.match("[a-zA-Z0-9]+\.[a-zA-Z0-9]{1,4}", url['uri'], re.M | re.I):
                url['uri'] = self.currentDomain + "/" + url['uri']
            elif url['uri'].startswith("/"):
                url['uri'] = self.currentDomain + url['uri']
            elif url['uri'].startswith("?"):
                url['uri'] = self.currentDomain + "/" + url['uri']
            elif url['uri'].startswith(".."):
                level = len(url['uri'].split('..')) - 1
                parent = url['parent'][0:len(url['parent']) - 1] if url['parent'].endswith('/') else url['parent']
                if parent.startswith('http://'): parent = parent[7:]
                parts = parent.split('/')
                final = 'http://'
                for i in range(0, len(parts) - level): final += parts[i] + '/'
                url['uri'] = final
            elif url['uri'].startswith("./"):
                url['uri'] = self.currentDomain + "/" + url['uri'][2:]
            else:
                url['uri'] = self.currentDomain + "/" + url['uri']
            if not self.db.in_visited(url['uri']):
                self.__add_internal(url)
            else:
                if self.config.debug: print 'in visited: ', url
                pass
        elif uri.startswith(self.shortDomain):
            if not self.db.in_visited(url['uri']):
                #print 'adding'
                self.__add_internal(url)
            else:
                pass
                if self.config.debug: print 'in visited: ', url
        else:
            #print 'external: ', url
            self.__add_external(url)

    ''' Adds lists of urls to the list '''
    def add_batch(self, urls):
        for url in urls: self.add(url)

    ''' Clears all queues '''
    def clear_all(self):
        self.db.clear_queue()
        self.db.clear_visited()

    ''' Returns short domain name without protocol and www prefix '''
    def __normalize(self, uri):
        a = uri
        if not uri: return
        if uri[:8] == 'https://':
            uri = uri[8:]
        elif uri[:7] == 'http://':
            uri = uri[7:]
        uri = uri.split('/')[0].split('?')[0].split('%')[0].split(':')[0].split('\\')[0]
        return uri

    def resume(self):
        return True if self.db.queue_size() > 0 else False

    ''' Returns a list of visited links '''
    def get_visited(self):
        return self.db.get_visited()

    ''' Compiles a report for the crawled site '''
    def report(self):
        l = self.db.get_report(self.currentDomain)
        r = ['uri, code, parent']
        for i in range(len(l)):
            line = l[i]['uri'] + ', '
            if l[i]['code']: line += str(l[i]['code']) + ', '
            else: line += ',, '
            if l[i]['parent']: line += l[i]['parent']
            r.append(line)
        return r

    def visited(self, url):
        self.db.visited(url['uri'])
        self.db.save(self.__get_full_domain(self.currentDomain), url)

    ''' Returns next link '''
    @synchronized(lock)
    def get_next(self):
        url = self.db.pop_queue()
        if url and url['uri']:
            url['changed'] = False
            return url
        else:
            if self.singleDomain: return
            self.db.clear_queue()
            self.db.clear_visited()
            url = self.db.next()
            if d:
                url['changed'] = True
                short = self.__normalize(url['uri'])
                if not Fetcher.is_404('http://' + short):
                    if short != url['uri']:
                        try: self.db.rename(url['uri'], 'http://' + short)
                        except: pass
                    conf.url = 'http://' + short
                    self.currentDomain = short
                    return url
            return url

    ''' Prints number of links crawled '''
    def stats(self):
        return self.db.stats()

    ''' Returns full domain name '''
    def __get_full_domain(self, url = None):
        if not url:
            if self.currentDomain.startswith("http://"): return self.currentDomain
            if self.currentDomain.startswith("www"): return "http://" + self.currentDomain
            else: return self.currentDomain
        else:
            if url.startswith("http://") or url.startswith("https://"): return url
            if url.startswith("www"): return "http://" + url
            else: return "http://" + url

    ''' Adds link to internal queue '''
    def __add_internal(self, url):
        #print 'add internal: ', url
        self.db.queue(url)
        

    ''' Adds link to external queue '''
    def __add_external(self, url):
        if self.config.debug: print 'add external: ', url
        url['uri'] = self.__get_full_domain(self.__normalize(url['uri']))
        self.db.save(url['uri'], url)
