from lib.db.redis_driver import Redis_Driver

class Database():

    @staticmethod
    def factory(params):
        if params['driver'] == 'redis':
            return Redis_Driver(params)
        else:
            raise NotImplementedError
