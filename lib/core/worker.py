import threading, time

from lib.core.extractor import Extractor
from lib.core.fetcher import Fetcher

class Worker(threading.Thread):

    def __init__(self, manager, queue, config):
        self.config = config
        self.start_time = time.time()
        self.minutes = 0
        self.is_running = True
        self.manager = manager
        self.queue = queue
        self.fetcher = Fetcher()
        threading.Thread.__init__(self)
 
    def run(self):
        while self.is_running:
            # check if there is a time limit
            if self.minutes > 0:
                elapsed = int(time.time() - self.start_time) / 60 # in minutes
                if self.minutes < elapsed: break

            # nope, continue
            url = self.manager.get_next()

            if not url or not url['uri']: break

            links = list()

            if url['changed']:
                # run crawl plugins
                if len(self.config.modules['crawl']) > 0:
                    for module in self.config.modules['crawl']:
                        links = list()
                        module.printInfo()
                        L = module.run()
                        for link in L: links.append({'uri': link, 'parent': self.config.url, 'code': 0}) 
                        self.manager.add_batch(links)

            # continue with normal stuff
            self.queue.put(url['uri'])

            data = self.fetcher.fetch(url['uri'], True, True)
            if data['content']:
                # run parse plugins here
                L = Extractor.extract_links(data['content'], True)
                for link in L: links.append({'uri': link, 'code': data['code'], 'parent': url['uri']})
                self.manager.add_batch(links)

            self.manager.visited({'uri': url['uri'], 'code': data['code'], 'parent': url['parent']})

    def stop(self):
        self.is_running = False

    def alarm(self, minutes):
        self.minutes = minutes
