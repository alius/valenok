import requests

class Fetcher():

    '''
    url - url to fetch
    split - split the contents by new lines or return as single string
    with_code - return http code
    '''

    '''
    @staticmethod
    def fetch(url, split = True, with_code = True):
        userAgent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.8.1.14) Gecko/20080404 Firefox/2.0.0.14'
        response = {'content': '', 'code': 0}

        if (url == '' or url == None):
            return response if with_code else response['content']

        if (not url.startswith("http://")): url = "http://" + url

        try:
            req = urllib2.Request(url)
            req.add_header('User-Agent', userAgent)
            try:
                httpConnection = urllib2.urlopen(req)
                response['content'] = httpConnection.read().replace('\n', '').replace('\r', '').replace('&amp;', '&') if split else httpConnection.read().replace('&amp;', '&')
                response['code'] = httpConnection.getcode()
                httpConnection.close()
            except urllib2.URLError, e:
                if hasattr(e, 'code'): response['code'] = e.code
                else: response['code'] = 0
        except Exception as inst:
            response['code'] = 0

        return response if with_code else response['content']
    '''

    def __init__(self):
        self.s = requests.session()

    def fetch(self, url, split = True, with_code = True):
        response = {'content': '', 'code': 0}

        if (url == '' or url == None):
            return response if with_code else response['content']

        if (not url.startswith("http://")): url = "http://" + url

        try:
            r = self.s.get(url)
            response['content'] = r.text.replace('\n', '').replace('\r', '').replace('&amp;', '&') if split else r.text.replace('&amp;', '&')
            response['code'] = r.status_code
        except Exception as inst:
            response['code'] = 0

        return response if with_code else response['content']

    @staticmethod
    def is_404(url):
        r = requests.head(url)
        return True if r.status_code > 199 and r.status_code < 300 else False

    @staticmethod
    def get(url):
        r = requests.get(url)
        return {'content': r.text, 'code': r.status_code, 'parent': ''}
