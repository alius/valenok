import Queue

from lib.core.timer import Timer
from lib.core.datatype import Configuration
from lib.core.manager import Manager

''' holds the crawler configuration '''
conf = Configuration()

''' Queues '''
urlQueue = Queue.Queue()
timeQueue = Queue.Queue()

''' Timer '''
timer = Timer(timeQueue)

manager = Manager()
