import urllib2, cookielib, os, socket, sys, Queue, time

from lib.core.data import conf
from lib.core.data import timer
from lib.core.data import manager
from lib.core.data import urlQueue
from lib.core.data import timeQueue

from lib.core.options import cmdArgParser
from lib.core.options import configParser
from lib.core.options import setupDefaults
from lib.core.database import Database
from lib.core.worker import Worker
from lib.core.fetcher import Fetcher
from lib.core.extractor import Extractor

from lib.UIthread import UIthread
from lib.report import Report

from modules.crawl.robots import Robots

def prepare():
    arguments = cmdArgParser()
    conf.init(arguments.__dict__)

    # parses cmd options and initializes necessary variables
    if conf.config:
        configParser()
    else:
        setupDefaults()

    # configure connection params
    socket.setdefaulttimeout(conf.timeout)
    cookieJar = cookielib.LWPCookieJar()
    if os.path.isfile('cookies.lwp'): cookieJar.load('cookies.lwp')
    opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cookieJar))
    urllib2.install_opener(opener)

    manager.setup(Database.factory({'driver': conf.driver, 'host': conf.host, 'port': conf.port, 'db': conf.name}), conf)

    if not conf.url.startswith("http://"): conf.url = "http://" + conf.url
    manager.set_domain(conf.url)
    manager.crawl_single(conf.single)

def start():
    timer.start()

    if conf.gui:
        ui = UIthread(urlQueue, timeQueue)
        ui.start()

    if not (conf.resume and manager.resume()):
        if conf.debug: print 'No resume. Cleaning queues...'

        # clear the visited queue
        manager.clear_all()

        if not conf.url.endswith('/'): conf.url = conf.url + '/'

        # run crawl plugins
        if len(conf.modules['crawl']) > 0:
            for module in conf.modules['crawl']:
                links = list()
                module.printInfo()
                L = module.run()
                for link in L: links.append({'uri': link, 'parent': conf.url, 'code': 0}) 
                manager.add_batch(links)

        # parse the index page
        urlQueue.put_nowait(conf.url)
        
        data = Fetcher.get(conf.url)

        if data['content']:
            links = list()
            L = Extractor.extract_links(data['content'])
            for link in L: links.append({'uri': link, 'parent': conf.url, 'code': 0})
            manager.add_batch(links)

        manager.visited({'uri': conf.url, 'code': data['code'], 'parent': None})

    if conf.debug: print 'Starting threads...'

    threads = list()
    for i in range(0, conf.threads):
        threads.append(Worker(manager, urlQueue, conf))
        threads[i].start()
        if conf.ttl and conf.ttl > 0: threads[i].alarm(conf.ttl)

    # otherwise wait for threads to finish normally
    for i in range(0, conf.threads):
        threads[i].join()

    if conf.report:
        report = Report.factory({'driver': conf.format, 'path': conf.path})
        if conf.format == 'csv':
            report.create(manager.report())
        elif conf.format == 'txt':
            report.create(manager.get_visited())

    timer.stop()
    timer.join()

    if conf.gui:
        ui.stop()
        ui.join()

def stop():
    sys.exit()
