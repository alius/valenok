class Configuration(object):
    def init(self, args):
        self.args = args

    def __getattr__(self, name):
        try:
            return self.args[name]
        except KeyError:
            print name
            raise AttributeError
