import sys, os
import ConfigParser

from optparse import OptionError
from optparse import OptionGroup
from optparse import OptionParser
from optparse import SUPPRESS_HELP

from lib.core.data import conf
from lib.core.data import urlQueue
from lib.core.data import timeQueue
from lib.core.data import timer

def setupDefaults():
    if not conf.threads: conf.threads = 1
    if not conf.timeout: conf.timeout = 10
    if conf.single is None: conf.single = True
    if conf.report is None: conf.report = False
    if conf.gui is None: conf.gui = False
    if conf.debug is None: conf.debug = False

def configParser():
    ''' Parses the config file '''
    c = ConfigParser.RawConfigParser()
    try:
        if os.path.isfile(conf.config): c.read(conf.config)
        else:
            print 'Cannot open config file'
            sys.exit()
    except:
        sys.exit()

    # database
    conf.driver = c.get('database', 'driver')
    conf.host = c.get('database', 'host')
    conf.name = c.get('database', 'db')
    conf.port = c.getint('database', 'port')

    if not any((conf.driver, conf.host, conf.name, conf.port)):
        print 'Missing database information'
        sys.exit()

    conf.url = c.get('domain', 'url')
    if not conf.url:
        print 'Missing target URL'
        sys.exit()

    conf.threads = c.getint('threading', 'count')
    if not conf.threads: conf.threads = 1

    conf.timeout = c.getint('socket', 'timeout')
    if not conf.timeout: conf.timeout = 10    

    conf.single = c.getboolean('crawler', 'single_domain')
    if conf.single is None: conf.single = True
    conf.fast_crawl = c.getboolean('crawler', 'fast') if c.has_option('crawler', 'fast') else False

    conf.report = c.getboolean('reporting', 'output')
    if conf.report is None: conf.report = False
    conf.format = c.get('reporting', 'driver')
    conf.path = c.get('reporting', 'path')
    if conf.report and not any((conf.format, conf.path)):
        print 'Report format or path is missing'
        sys.exit()

    # Misc
    conf.gui = c.getboolean('misc', 'gui')
    if conf.gui is None: conf.gui = True

    conf.resume = c.getboolean('misc', 'resume')
    if conf.resume is None: conf.resume = True

    conf.modules = {'parse': list(), 'crawl': list()}
    modules = c.get('misc', 'modules') if c.has_option('misc', 'modules') else None
    if modules:
        for module in modules.split(','):
            module = 'modules.' + module
            tmp = module.split('.')
            name = tmp[len(tmp) - 1].title()
            mod = __import__(module, fromlist=[name])
            cl = getattr(mod, name)
            if module.startswith('modules.parse'):
                conf.modules['parse'].append(cl(conf))
            else:
                conf.modules['crawl'].append(cl(conf))

    conf.dirs_wordlist = c.get('misc', 'dirs_wordlist') if c.has_option('misc', 'dirs_wordlist') else None
    conf.debug = c.getboolean('misc', 'debug') if c.has_option('misc', 'debug') else False
    conf.ttl = c.getfloat('misc', 'ttl') if c.has_option('misc', 'ttl') else None

def cmdArgParser():
    '''
    Parse command line arguments
    '''
    usage = "%s [options]" % sys.argv[0]

    parser = OptionParser(usage=usage)

    try:
        target = OptionGroup(parser, "Target", "Target URL to crawl")

        target.add_option("-u", "--url", dest="url", help="Target url")
        target.add_option("-c", "--config", dest="config", help="Configuration file")

        database = OptionGroup(parser, "Database", "Database configuration")

        database.add_option("--db-driver", dest="driver", help="Database engine, i.e. redis")
        database.add_option("--db-host", dest="host", help="Database host address")
        database.add_option("--db-port", type="int", dest="port", help="Database port")
        database.add_option("--db-name", dest="name", help="Database name")

        threading = OptionGroup(parser, "Threading", "Thread configuration")

        threading.add_option("--threads", type="int", dest="threads", help="Number of threads to spawn")

        socket = OptionGroup(parser, "Socket", "Tcp timeout option")

        socket.add_option("--timeout", type="int", dest="timeout", help="Tcp timeout")

        crawler = OptionGroup(parser, "Crawler", "Crawler specific configuration")

        crawler.add_option("--single-domain", action="store_true", dest="single", help="Parse only single domain")

        reporting = OptionGroup(parser, "Reporting", "Report generation")

        reporting.add_option("--generate-report", action="store_true", dest="report", help="Generate report with crawled URLs")
        reporting.add_option("--report-format", dest="format", help="Which format to save the report in")
        reporting.add_option("--report-path", dest="path", help="Where to save the report")

        misc = OptionGroup(parser, "Miscellaneous", "Miscellaneous options")

        misc.add_option("--enable-gui", action="store_true", dest="gui", help="Show the fancy curses GUI or print directly to console")
        misc.add_option("--resume", action="store_true", dest="resume", help="Resume the last scan that did not finish")
        misc.add_option("--enable-debug", action="store_true", dest="debug", help="Number of minutes for the crawler to work")
        misc.add_option("--ttl", dest="ttl", help="Number of minutes for the crawler to work")
        misc.add_option("--modules", dest="modules", help="What modules to use")
        misc.add_option("--dirs-wordlist", dest="dirs_wordlist", help="Dictionary file for dirs module")

        parser.add_option_group(target)
        parser.add_option_group(database)
        parser.add_option_group(threading)
        parser.add_option_group(socket)
        parser.add_option_group(crawler)
        parser.add_option_group(reporting)
        parser.add_option_group(misc)

        args = []

        for arg in sys.argv:
            args.append(arg)

        (args, _) = parser.parse_args(args)

        if not any((args.url, args.config, args.driver)):
            parser.error("Missing required options")

        return args

    except (OptionError, TypeError), e:
        parser.error(e)

    except SystemExit, _:
        raise
