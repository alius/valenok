from reporting import Txt

class Report():

    @staticmethod
    def factory(params):
        if params['driver'] == 'txt':
            return Txt.Txt(params)
        elif params['driver'] == 'csv':
            return Txt.Txt(params)
        else:
            raise NotImplementedError
